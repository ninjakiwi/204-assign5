import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.DatagramPacket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ChatClient {
	public static void main(String args[]) {

		int port = 40202;
		String multicastGroupIP = "239.0.202.1";

		// Try to connect to the multicast group
		InetAddress group;
		MulticastSocket mcs;
		try {
			group = InetAddress.getByName(multicastGroupIP);
			mcs = new MulticastSocket(port);
			mcs.joinGroup(group);
		} catch (UnknownHostException e) {
			System.out.println("Failed to find multicast group address");
			return;
		} catch (IOException e) {
			System.out.println("Failed to connect to multicast group");
			return;
		}

		ListenerThread listenerThread = new ListenerThread(mcs);
		listenerThread.start();

		Scanner input = new Scanner(System.in).useDelimiter("\n");

		while (true) {
			String msg = input.next();
			DatagramPacket msgPacket = new DatagramPacket(msg.getBytes(), msg.length(), group, port);

			try {
				mcs.send(msgPacket);
			} catch(IOException e) {
				System.out.println("Failed to send message");
			}

		}
	}
}

class ListenerThread extends Thread {

	private MulticastSocket socket;

	public ListenerThread(MulticastSocket mcs) {
		socket = mcs;
	}

	public void run() {

		// get incomming traffic
		while (true) {
			byte[] buf = new byte[2000];
			DatagramPacket recv = new DatagramPacket(buf, buf.length);

			try {
				socket.receive(recv);
			} catch (IOException e) {
				System.out.println("Failed to recieve data");
			}

			// Get sender address
			SocketAddress​ recvSocAddr = recv.getSocketAddress();
			InetSocketAddress sa = (InetSocketAddress)recvSocAddr;

			byte[] packetData = recv.getData();
			int packetLen = recv.getLength();

			String msg = new String(packetData, StandardCharsets.UTF_8);
			
			// Remove majority of wierd control characters
			msg = msg.replaceAll("[\u0000-\u001F]", "");

			System.out.println(sa.getHostName() + ": " + msg);
		}
	}
}
